let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}


let queue = [];
function enqueue(element) {
    const newQueue = [];
    for (let i = 0; i < queue.length; i++) {
        newQueue[i] = queue[i];
    }
    newQueue[newQueue.length] = element;
    queue = newQueue;
    return newQueue;
}


function dequeue() {
    for (let i = 0; i < queue.length - 1; i++) {
    queue[i] = queue[i + 1];
  }
    queue.length--
    return queue;
  }


function front() {
    return queue[0]
}

function size() {
    return queue.length
}

function isEmpty() {
    if (queue.length !== 0){
        return false
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};